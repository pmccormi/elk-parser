"use strict";
const dgram = require('dgram');
const loraPacket = require('lora-packet')
const Logstash = require('logstash-client');

const server = dgram.createSocket('udp4');
const headerSize = 12;
const macAddressHeaderOffset = 4;
const jsonStart = '{';
var logstash = new Logstash({
    type: 'udp',
    host: 'logstash',
    port: 2000
});

server.on('listening', () => {
    const address = server.address();
    console.log(`server listening ${address.address}:${address.port}`);
});

server.on('error', (err) => {
    console.log(`server error:\n${err.stack}`);
    server.close();
});

function isPacketValid(packet) {
    if (packet.length <= headerSize) {
        return false
    }
    return true
}

function packetToJson(packet) {
    // Slice when it's a buffer since the '{' character can appear when you do .toString()
    // which might break the .indexOf()
    let packetString = packet.slice(headerSize - 1).toString()
    let msg = packetString.substring(packetString.indexOf(jsonStart))
    return JSON.parse(msg);
}

function prepareInitalData(logstashData, json, rawMsg) {
    logstashData.Raw_pckt = JSON.stringify(json) // Have to stringify because previous version it was a text....WHY
    logstashData.GW_EUI = rawMsg.slice(macAddressHeaderOffset, headerSize).toString('hex')
}

function addToPayloadIfExists(loraDecodedPacket, logstashData, bufferName, keyName) {
    let buffers = loraDecodedPacket.getBuffers()
    if (buffers[bufferName]) {
        logstashData[keyName] = buffers[bufferName].toString('hex')
    }
}

function parseSfAndBw(logstashData, packet) {
    if (packet.datr) {
        // The packet.datr is of form SFXXBWXXX
        // where the X's are numbers
        // SF: Spreading factor
        // BW: Bandwidth
        let SfBw = packet.datr.split("SF")[1].split("BW")
        if (SfBw.length == 2) {
            logstashData.SF = parseInt(SfBw[0])
            logstashData.Bandwidth = parseInt(SfBw[1])
        }
    }
}

function deepCopy(obj) {
    return JSON.parse(JSON.stringify(obj))
}

function isPacketBitrateCalculable(packet) {
    // If there is no coding rate, then it was never sent on the air
    // If stat is not at 1, there is no payload
    if (packet.codr != "OFF" && packet.stat == 1) {
        return true;
    }
    return false;
}

// If nothing makes sense in this, check DaemonELK.php
// That's where I took most of this code from
// I also don't know how to make sense of it
function calculateBitrateAndAirtime(packet, logstashData) {
    // packet.codr is of format X/X
    // where X are numbers
    let redundantCodingRate = parseInt(packet.codr.split("/")[1])

    let crc = packet.stat
    let symbolTime = Math.pow(2, logstashData.SF) / logstashData.Bandwidth
    let implicitHeader = logstashData.SF == 6 ? 1 : 0
    let lowDatarateOptimization = 1
    if (symbolTime < 16) {
        lowDatarateOptimization = 0
    }
    let preemptiveTime = (8 + 4.25) * symbolTime // I legit don't know what 8 and 4.25 are
    let payloadSize = packet.size
    let payloadLength = 8 + Math.max(
        Math.ceil(
            (8 * payloadSize - 4 * logstashData.SF + 28 + 16 * crc - 20 * implicitHeader) / (4 * (logstashData.SF - 2 * lowDatarateOptimization)) * redundantCodingRate
        ), 0)
    let payloadTime = payloadLength * symbolTime
    logstashData.Airtime = preemptiveTime + payloadTime
    logstashData.BitRate = ((4 / redundantCodingRate) / symbolTime) * 1000 * logstashData.SF
}

function parseRxPacket(logstashData, json) {
    json.rxpk.forEach(packet => {
        let logstashPacketData = deepCopy(logstashData)
        logstashPacketData.rxpk = packet
        let loraDecodedPacket = loraPacket.fromWire(Buffer.from(packet.data, "base64"))
        logstashPacketData.Type = loraDecodedPacket.getMType()
        addToPayloadIfExists(loraDecodedPacket, logstashPacketData, "DevEUI", "Dev_EUI")
        addToPayloadIfExists(loraDecodedPacket, logstashPacketData, "DevAddr", "Dev_Add")
        addToPayloadIfExists(loraDecodedPacket, logstashPacketData, "MIC", "MIC")
        logstashPacketData.Coding_rate = packet.codr
        parseSfAndBw(logstashPacketData, packet)

        if (isPacketBitrateCalculable(packet)) {
            calculateBitrateAndAirtime(packet, logstashPacketData)
        }
        sendToLogstash(logstashPacketData)
    });
    console.log(`Parsed RX packet with ${json.rxpk.length} packet${json.rxpk.length > 1 ? "s" : ""}`)
}

function parseStatPacket(logstashData) {
    logstashData.Type = "Stat";
    sendToLogstash(logstashData)
    console.log("Parsed Stat packet")
}

function parseUnknownPacket(logstashData) {
    logstashData.Type = "Unknown";
    sendToLogstash(logstashData)
    console.log("Parsed unknown packet")
}

function sendToLogstash(payload) {
    logstash.send(payload)
}

server.on('message', (msg, rinfo) => {
    if (isPacketValid(msg)) {
        let json = packetToJson(msg)
        let logstashData = {}

        prepareInitalData(logstashData, json, msg)

        if (json.rxpk) {
            parseRxPacket(logstashData, json)
        }
        else if (json.stat) {
            parseStatPacket(logstashData)
        }
        else {
            parseUnknownPacket(logstashData)
        }
    }
    else {
        console.log("Ignored packet with only header")
    }
});

server.bind(1800);