#!/usr/bin/env php
<?php

//Fonction pour décoder plusieurs JSON séparés par des virgules/autre chose

function json_decode_multi($s, $assoc = false, $depth = 512, $options = 0) {
    if(substr($s, -1) == ',')  //changer la virgule par le caractère de séparation
        $s = substr($s, 0, -1);
    return json_decode("[$s]", $assoc, $depth, $options);
}


//Reduction erreurs
error_reporting(~E_WARNING);

//Creation socket UDP
if(!($sock = socket_create(AF_INET, SOCK_DGRAM, 0)))
{
    $errorcode = socket_last_error();
    $errormsg = socket_strerror($errorcode);

    die("Couldn't create socket: [$errorcode] $errormsg \n");
}

// Allocation adresse source
if( !socket_bind($sock, "0.0.0.0" , 1800) )
{
    $errorcode = socket_last_error();
    $errormsg = socket_strerror($errorcode);

    die("Could not bind socket : [$errorcode] $errormsg \n");
}


$sock2 = socket_create(AF_INET, SOCK_DGRAM, SOL_UDP);
//Boucle d'attente de communication
while(1)
{
	a:     // Point de retour si le message est pas assez long (juste ça E.......@...@.....1 )
    //Reception données
    $r = socket_recvfrom($sock, $buf, 1024, 0, $remote_ip, $remote_port);
    $json=substr($buf, 12);
    $XbufEUI = substr(bin2hex($buf), 8,16);

    if(strlen($buf) <= 12){
    	goto a;
    }
    /*---------------------------Début du code-----------------------------*/

	$logstash_obj= new stdClass();
	$logstash_obj-> Raw_pckt = $json;
	$logstash_obj-> GW_EUI= $XbufEUI;

	$decoded_json = json_decode_multi($json,JSON_OBJECT_AS_ARRAY); //Application de la fonction de décodage pour retourner un JSON sous forme de tableau

	if($decoded_json [0]['rxpk']){
		/*-----------------------------------PAQUETS RX-------------------------------------------------------------------------------*/
		$payload_decoder='/media/ELK/Scripts/LoraDecode/lora-packet-decode'; //Chemin d'accès pour le script Perl qui sert à décoder
		$NBR_PKT = sizeof($decoded_json[0]['rxpk']);

		for($i=0;$i<=($NBR_PKT - 1);$i++){ //Si plusieurs paquets à l'intérieur de la réception, on les parcourt pour tout extraire

			/*------Infos relatives au message brut sans prise en compte de la Gateway qui l'a capté-------*/

			$PAYLOAD[$i] = $decoded_json [0]['rxpk'][$i]['data'];

			/*-------Décodage du payload------------*/

			exec("sudo $payload_decoder --base64 $PAYLOAD[$i]",$D_Payload[$i]);

			/*-----------------Suivant le type de paquet, on a différents traitements (infos à l'intérieur pas formatées pareilles)---------------------*/

			if(strstr($D_Payload[$i][3],'Message Type = Join Request') != FALSE){

				$logstash_obj-> Dev_EUI = substr($D_Payload[$i][13],25);
				$logstash_obj-> MIC = substr($D_Payload[$i][9],25);
				if($decoded_json[0]['rxpk'][$i]['modu'] == "LORA"){
					$logstash_obj-> Modulation = "LoRaWAN";
				}elseif($decoded_json[0]['rxpk'][$i]['modu'] == "FSK"){
					$logstash_obj-> Modulation = "LoRaWAN";
				}
				$logstash_obj-> Type = "Join Request";

			}elseif(strstr($D_Payload[$i][3],'Message Type = Data') != FALSE){

				$logstash_obj-> Dev_Add = substr($D_Payload[$i][17],25,8);
	           		$logstash_obj-> MIC = substr($D_Payload[$i][9],25);
				if($decoded_json[0]['rxpk'][$i]['modu'] == "LORA"){
					$logstash_obj-> Modulation = "LoRaWAN";
				}elseif($decoded_json[0]['rxpk'][$i]['modu'] == "FSK"){
					$logstash_obj-> Modulation = "LoRaWAN";
				}
				$logstash_obj-> Type = "Data";

				/*-----------------------Conditions sur l'obtention des données GPS-----------------------------*/
				if($Dev_Add == 26011817){ // Changer la Dev Address si ce n'est pas celle utilisée

					/*---------------------Valeurs des clés à changer suivant l'appli utilisée----------------------------*/
					$NWKKEY = "8F58A260F76CE0A2104BA65AA678CC37";
					$APPKEY = "0173B7DA3AFDD6129093BE3DD1C5D303";
					/*------------------------------------------------------------------------*/
					exec("sudo $payload_decoder --appkey $APPKEY --nwkkey $NWKKEY --base64 $PAYLOAD[$i]",$D_Payload[$i]);
					$logstash_obj-> Appartenance = "Nous";
					$logstash_obj-> GPSHex = substr($D_Payload[$i][15],25,38);
				}

			}else{
				if($decoded_json [0]['rxpk'][$i]['stat'] != -1){

					$logstash_obj-> Modulation = "LoRa";
					$logstash_obj-> Type = "Data";
				}
			}
			$logstash_obj-> RxTx = "RX";

			/*------------------------Récupération d'infos relatives à l'envoi du paquet----------------------------*/

			preg_match_all('!\d+!',$decoded_json [0]['rxpk'][$i]['datr'], $matches); // Récupération des données de Spreading factor & Bandwidth sous forme de tableau
			$SF[$i] = $matches [0][0];
			$BANDWIDTH[$i] = $matches [0][1];

			$CODING_RATE[$i] = $decoded_json [0]['rxpk'][$i]['codr']; // Récupération de la donnée de Coding Rate sous forme de tableau
			if ($CODING_RATE[$i]=="OFF"){
				$AIRTIME = null ;
				$BIT_RATE = null ;
				goto f; // si coding rate vaut OFF, pas de calcul possible donc skip

			}
			preg_match_all('!\d+!',$CODING_RATE[$i], $denom);
			$DENOM_CODING_RATE[$i] = $denom [0][1];

			/*------Calcul du Airtime pour chaque Gateway ayant capté le message------*/

			$T_SYMBOL[$i] = pow(2,$SF[$i])/$BANDWIDTH[$i]; // Temps de symbole, inverse du taux de symbole

			/*------Paramètres préliminaires du paquet (par défaut)------*/

			$CRC = $decoded_json [0]['rxpk'][$i]['stat']; // Vaut 1 si présence de Payload, 0 sinon, valeur de rxpk.stat
			if($CRC != 1){
				$AIRTIME = null ;
				$BIT_RATE = null ;
				goto f;
			}
			$IH = 0; // Vaut 1 si Header implicite, 0 sinon, 	0 pour du LoRaWAN car c'est le header qui donne coding rate, longueur de payload et CRC

			if ($T_SYMBOL[$i] < 16){
				$LDO = 0; // Vaut 1 si Low Data Rate Optimization activé, 0 sinon, si T_symbol>16 ms actif -> automatiquement actif pour SF11 et 12 en BW 125
			}else{
				$LDO = 1;
			}

			if ($SF == 6) {
				$IH = 1;	// Si le Spreading Factor vaut 6, le header est automatiquement implicite, fait partie de la modu LoRa mais inutilisé en LoRaWAN
			}
			/*------Fin des Paramètres------*/

			$T_PREAMBLE[$i] = (($LEN_PREAMBLE[$i] = 8) + 4.25) * $T_SYMBOL[$i]; // Choisir la valeur de longueur du préambule, 8 pour du LoRaWAN 1.0 selon les données officielles

			$PAYLOAD_SIZE[$i] = $decoded_json [0]['rxpk'][$i]['size']; // Récupération du nombre d'octet du payload

			$LEN_PAYLOAD[$i] = 8 + max(ceil((8*$PAYLOAD_SIZE[$i] - 4*$SF[$i] + 28 + 16*$CRC - 20*$IH)/(4*($SF[$i]-2*$LDO))*$DENOM_CODING_RATE[$i]),0); // Calcul de la longueur du Payload = son nombre de symboles

			$T_PAYLOAD[$i] = $LEN_PAYLOAD[$i] * $T_SYMBOL[$i]; // Calcul du temps de Payload

			$AIRTIME[$i] = $T_PREAMBLE[$i] + $T_PAYLOAD[$i]; // Calcul final du Airtime

			/*-----------------------Calcul du Bit Rate-------------------------*/

			$BIT_RATE[$i] = ((4/$DENOM_CODING_RATE[$i])/$T_SYMBOL[$i])*1000*$SF[$i];


			f: //Failure of calculations
			/*--------------------Remplissage du JSON (again)------------------------*/

				$logstash_obj-> SF = $SF[$i];
		        $logstash_obj-> Bandwidth = $BANDWIDTH[$i];
		        $logstash_obj-> Coding_rate = $CODING_RATE[$i];
		        $logstash_obj-> Airtime = $AIRTIME[$i];
		        $logstash_obj-> BitRate = $BIT_RATE[$i];

			/*-----------Création du JSON avec la cmd---------------*/

			$logstash_JSON = json_encode($logstash_obj);

			/*------------Envoi du JSON à logstash----------*/

			$len = strlen($logstash_JSON);

			socket_sendto($sock2, $logstash_JSON, $len, 0, '172.23.0.191', 2000);
			unset($D_Payload[$i]); //on vide la variable
		}
	} elseif($decoded_json [0]['stat']) {

		$logstash_obj-> Type = "Stat";
		$logstash_JSON = json_encode($logstash_obj);
	    $len = strlen($logstash_JSON);
	    socket_sendto($sock2, $logstash_JSON, $len, 0, '172.23.0.191', 2000);

	} elseif($decoded_json [0]['txpk']) {

	    /*-------------------------------------PAQUETS TX------------------------------------------------------*/
		$payload_decoder='/home/rducray/Documents/LoraDecode/lora-packet-decode'; //Chemin d'accès pour le script Perl qui sert à décoder

		$PAYLOAD = $decoded_json [0]['txpk']['data'];
		exec("sudo $payload_decoder --base64 $PAYLOAD",$D_Payload);

		if(strstr($D_Payload[3],'Message Type = Data') != FALSE){

			$logstash_obj-> Dev_Add = substr($D_Payload[17],25,8);
		    $logstash_obj-> MIC = substr($D_Payload[9],25);
		    $logstash_obj-> Modulation = $Type;
		    if($decoded_json[0]['rxpk'][$i]['modu'] == "LORA"){
				$logstash_obj-> Modulation = "LoRaWAN";
		    }elseif($decoded_json[0]['rxpk'][$i]['modu'] == "FSK"){
				$logstash_obj-> Modulation = "LoRaWAN";
	            }

		} elseif (strstr($D_Payload[3],'Message Type = Join Accept') != FALSE){

			$logstash_obj-> Dev_Add = substr($D_Payload[15],25,8);
		    $logstash_obj-> MIC = substr($D_Payload[10],25);
		    $logstash_obj-> Modulation = $Type;
		    if($decoded_json[0]['rxpk'][$i]['modu'] == "LORA"){
				$logstash_obj-> Modulation = "LoRaWAN";
		     }elseif($decoded_json[0]['rxpk'][$i]['modu'] == "FSK"){
				$logstash_obj-> Modulation = "LoRaWAN";
		     }
		}
		$logstash_obj-> RxTx = "TX";

		preg_match_all('!\d+!',$decoded_json [0]['txpk']['datr'], $matches); // Récupération des données de Spreading factor & Bandwidth sous forme de tableau
		$SF = $matches [0][0];
		$BANDWIDTH = $matches [0][1];

		$CODING_RATE = $decoded_json [0]['txpk']['codr']; // Récupération de la donnée de Coding Rate sous forme de tableau
		preg_match_all('!\d+!',$CODING_RATE, $denom);
		$DENOM_CODING_RATE = $denom [0][1];

		/*------Calcul du Airtime pour chaque Gateway ayant capté le message------*/

		$T_SYMBOL = pow(2,$SF)/$BANDWIDTH; // Temps de symbole, inverse du taux de symbole

			/*------Paramètres préliminaires du paquet (par défaut)------*/

			$CRC = 1; // Vaut 1 si présence de Payload, 0 sinon, valeur de rxpk.stat
			$IH = 0; // Vaut 1 si Header implicite, 0 sinon, 	0 pour du LoRaWAN car c'est le header qui donne coding rate, longueur de payload et CRC

			if ($T_SYMBOL < 16){
				$LDO = 0; // Vaut 1 si Low Data Rate Optimization activé, 0 sinon, si T_symbol>16 actif -> automatiquement actif pour SF11 et 12 et BW 125
			}else{
				$LDO = 1;
			}

			if ($SF == 6) {
				$IH = 1;	// Si le Spreading Factor vaut 6, le header est automatiquement implicite, fait partie de la modu LoRa mais inutilisé en LoRaWAN
			}
			/*------Fin des Paramètres------*/

		$T_PREAMBLE = (($LEN_PREAMBLE = 8) + 4.25) * $T_SYMBOL; // Choisir la valeur de longueur du préambule, 8 pour du LoRaWAN 1.0 selon les données officielles

		$PAYLOAD_SIZE = $decoded_json [0]['txpk']['size']; // Récupération du nombre d'octet du payload

		$LEN_PAYLOAD = 8 + max(ceil((8*$PAYLOAD_SIZE - 4*$SF+ 28 + 16*$CRC - 20*$IH)/(4*($SF-2*$LDO))*$DENOM_CODING_RATE),0); // Calcul de la longueur du Payload = son nombre de symboles

		$T_PAYLOAD = $LEN_PAYLOAD * $T_SYMBOL; // Calcul du temps de Payload

		$AIRTIME = $T_PREAMBLE + $T_PAYLOAD; // Calcul final du Airtime

		/*-----------------------Calcul du Bit Rate-------------------------*/

		$BIT_RATE = ((4/$DENOM_CODING_RATE)/$T_SYMBOL)*1000*$SF;

		/*----------------------Remplissage du JSON (again)----------------------*/

		$logstash_obj-> SF = $SF;
		$logstash_obj-> Bandwidth = $BANDWIDTH;
		$logstash_obj-> Coding_rate = $CODING_RATE;
		$logstash_obj-> Airtime = $AIRTIME;
		$logstash_obj-> BitRate = $BIT_RATE;

		/*-----------Création du JSON avec la cmd---------------*/

		$logstash_JSON = json_encode($logstash_obj);

		/*------------------Envoi du JSON à Logstash-----------------*/

		$len = strlen($logstash_JSON);
		socket_sendto($sock2, $logstash_JSON, $len, 0, '172.23.0.191', 2000);
		unset($D_Payload); //on vide la variable
	}
}
socket_close($sock2);
socket_close($sock);

?>
